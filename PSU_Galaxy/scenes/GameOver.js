import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.11/firebase-app.js";
import { getDatabase, ref, set , onValue , query ,get ,orderByChild,limitToLast} from "https://www.gstatic.com/firebasejs/9.6.11/firebase-database.js";
class GameOver extends Phaser.Scene {
    constructor() {
        super('GameOver');
    }

    init(data) {
        this.GameOverMessage = data.GameOver_message;
        this.score = data.score
        var oldData = []
        var coutdo = 0
        var idStuden = '6310210151'
        


        // TODO: Add SDKs for Firebase products that you want to use
        // https://firebase.google.com/docs/web/setup#available-libraries
      
        // Your web app's Firebase configuration
        // For Firebase JS SDK v7.20.0 and later, measurementId is optional
        const firebaseConfig = {
          apiKey: "AIzaSyCd030zh9hCg7EHKMzmeB_R4UBB5-0pGME",
          authDomain: "psu-app-b5ebc.firebaseapp.com",
          databaseURL: "https://psu-app-b5ebc-default-rtdb.asia-southeast1.firebasedatabase.app",
          projectId: "psu-app-b5ebc",
          storageBucket: "psu-app-b5ebc.appspot.com",
          messagingSenderId: "1005392053184",
          appId: "1:1005392053184:web:98fcbe9dda76153bcfbe25",
          measurementId: "G-TDMNTRWLF1"
        };
      
        // Initialize Firebase
        
        const app = initializeApp(firebaseConfig);

        const db = getDatabase();

        const starCountRef = ref(db,'TEST/'+idStuden);

        onValue(starCountRef,(snapshot) => {
            oldData = snapshot.val()
            console.log(oldData)
            if(oldData ==null){
                InsertData(idStuden,this.score)
            }else{
                if(this.score > oldData.Score ){
                    console.log("True")
                    InsertData(idStuden,this.score)
                }
            }

        })

        function GetAllData(){
            const que = query(ref(db,"TEST"), orderByChild("Score"),limitToLast(10));
        
            get(que)
            .then((snapshot)=>{
                var top = [];
                snapshot.forEach(childSnapshot =>{
                    top.push(childSnapshot.val());
                })
                console.log(top);
            })
        }


        // ---------- Insert Data ------------//
        function InsertData(StudenID,score) {
            set(ref(db,"TEST/"+StudenID),{
                Name : "TANAKUY",
                Score : score,
                StudenID : "6310210151"
            })
            
        }
        // ---------------------------------- //

    }

    addGameOver() {
        this.gameoverHUD = this.add.text(this.width/2 - 200, this.height - 950, "Game Over", 
            {fontSize: '70px', fill: '#FF0000', fontWeight: 'bold', stroke: '#FF0000', strokeThickness: 6});
    }

    addScore() {
        this.gameoverHUD = this.add.text(this.width/2 - 150, this.height - 850, 'YOUR ' + this.GameOverMessage, 
            {fontSize: '32px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})   
    }

    addLeaderBoard() {
        const db = getDatabase();
        const que = query(ref(db,"TEST"), orderByChild("Score"),limitToLast(10));
        
            get(que)
            .then((snapshot)=>{
                var top = [];
                snapshot.forEach(childSnapshot =>{
                    top.push(childSnapshot.val());
                })
                console.log(top);
            
                
            
        this.leaderBoard = this.add.text(this.width/2 - 225, this.height - 800, 'RANK     SCORE     NAME', 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard1 = this.add.text(this.width/2 - 225, 50 + this.height - 800, '  1       '+top[9].Score+'     '+top[9].Name,
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard2 = this.add.text(this.width/2 - 225, 100 + this.height - 800, '  2       '+top[8].Score+'      '+top[8].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard3 = this.add.text(this.width/2 - 225, 150 + this.height - 800, '  3       '+top[7].Score+'      '+top[7].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard4 = this.add.text(this.width/2 - 225, 200 + this.height - 800, '  4       '+top[6].Score+'      '+top[6].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard5 = this.add.text(this.width/2 - 225, 250 + this.height - 800, '  5       '+top[5].Score+'      '+top[5].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard6 = this.add.text(this.width/2 - 225, 300 + this.height - 800, '  6       '+top[4].Score+'      '+top[4].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard7 = this.add.text(this.width/2 - 225, 350 + this.height - 800, '  7       '+top[3].Score+'      '+top[3].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard8 = this.add.text(this.width/2 - 225, 400 + this.height - 800, '  8       '+top[2].Score+'      '+top[2].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard9 = this.add.text(this.width/2 - 225, 450 + this.height - 800, '  9       '+top[1].Score+'      '+top[1].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        this.leaderBoard10 = this.add.text(this.width/2 - 225, 500 + this.height - 800, ' 10       '+top[0].Score+'      '+top[0].Name, 
            {fontSize: '30px', fill: '#FFF', fontWeight: 'bold', stroke: '#FFF', strokeThickness: 3})
        })

        }
        

    addBGM() {
        this.bgm = this.sound.add('GameOver', {volume: 0.05})
        this.bgm.play({loop: true})
    }

    addCilck() {
        this.click = this.sound.add('click', {volume: 0.25})
        this.click.play()
    }

    preload() {
        this.sceneStopped = false
        this.width = this.game.screenBaseSize.width
        this.height = this.game.screenBaseSize.height
        this.handlerScene = this.scene.get('handler')
        this.handlerScene.sceneRunning = 'GameOver'
    }

    create() {
        const { width, height } = this
        
        // CONFIG SCENE         
        this.handlerScene.updateResize(this)
        if (this.game.debugMode) {
            this.add.image(width/2, height/2, 'space').setOrigin(.5)
        }
        // CONFIG SCENE 

        //Text Creation
        this.addGameOver()
        this.addScore()
        this.addLeaderBoard()

        // Play Again!
        this.playagain1 = this.add.image(width/2 - 200, height/2 + 250, 'PlayAgain1').setOrigin(.1)
        this.playagain1.setScale(1, 1)
        this.playagain2 = this.add.image(width/2 - 200, height/2 + 250, 'PlayAgain2').setOrigin(.1)
        this.playagain2.setScale(1, 1)

        this.playagain1.setInteractive();
        this.playagain2.setInteractive();
        this.playagain1.on('pointerover', () => {
            this.playagain1.setVisible(false)
            this.playagain2.setVisible(true)
        })
        
        this.playagain2.on('pointerout', () => {
            this.playagain1.setVisible(true)
            this.playagain2.setVisible(false)
        })

        this.playagain2.on('pointerdown', () => {
            // Sound stop all
            this.sound.stopAll()

            // Click sound
            this.click.play()

            this.scene.start('title')
        })

        // BGM
        this.addBGM()
        this.addCilck()
    }

    update() {
        // this.player.rotation += 0.05
    }
}

export default GameOver